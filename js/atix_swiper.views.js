/**
 * @file atix_swiper.views.js
 *
 * Contains the behavior, that initializes Swiper on views using the views swiper style
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  Drupal.atixSwiper = Drupal.atixSwiper || {

    /**
     * Holds all Swiper instances on a page.
     */
    swiperInstances: {}

  };

  if (drupalSettings.atix_swiper) {
    $.each(drupalSettings.atix_swiper, function (index, value) {
//      console.log('#' + index, value.swiper);
      Drupal.atixSwiper.swiperInstances[index] = new Swiper(
        '#' + index,
        value.swiper
      );
    });
  }
//  var swiper = new Swiper('.swiper-container', {
//    pagination: '.swiper-pagination',
//    paginationClickable: true
//  });
}(jQuery, Drupal, drupalSettings));