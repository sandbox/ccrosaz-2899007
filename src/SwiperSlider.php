<?php

namespace Drupal\atix_swiper;

use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\views\ViewExecutable;
use Drupal\atix_swiper\Entity\SwiperOptionSet;

/**
 * The primary class for the Views ResponsiveBP module.
 *
 * Provides many helper methods.
 *
 * @ingroup utility
 */
class SwiperSlider {

  /**
   * Returns the theme hook definition information.
   */
  public static function getThemeHooks() {
    $base = [
      'file' => 'atix_swiper.theme.inc',
    ];
    $hooks['views_swiper_slider'] = $base + [
      'preprocess functions' => [
        'template_preprocess_views_swiper_slider',
        'template_preprocess_views_view_unformatted',
      ],
    ];
    $hooks['images_swiper_formatter'] = $base + [
      'render element' => 'element',
    ];
    $hooks['image_swiper_formatter'] = $base + [
      'variables' => [
        'item' => NULL,
        'item_attributes' => NULL,
        'url' => NULL,
        'image_style' => NULL,
        'caption' => FALSE
      ],
    ];
    $hooks['reference_swiper_formatter'] = $base + [
      'render element' => 'element',
    ];

    return $hooks;
  }

  /**
   * Get unique element id for view element.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   A ViewExecutable object.
   *
   * @return string
   *   A unique id for an HTML element.
   */
  public static function getUniqueViewId(ViewExecutable $view) {
    return self::getUniqueId($view->storage->id() . '-' . $view->current_display, 'views');
  }

  /**
   * Get unique element id for swiper element.
   *
   * @param string name
   *   The name of the element
   * @param string type
   *   The type of the element (views, field)
   * 
   * @return string
   *   A unique id for an HTML element.
   */
  public static function getUniqueId($name, $type) {
    return Html::getUniqueId($type . '-swiper-' . $name);
  }

  /**
   * Get clean CSS class for view.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   A ViewExecutable object.
   *
   * @return string
   *   A clean CSS class for the view current display.
   */
  public static function getViewClass(ViewExecutable $view) {
    return Html::cleanCssIdentifier('view-' . $view->current_display);
  }

  /**
   * Returns an entity_autocomplete element for settings form.
   *
   * @param string value
   *   The default value of the form element
   * 
   * @return array
   *   Render array of element for setting select.
   */
  public static function getSettingForm($value) {
    // Check whether any option sets are available.
    if (SwiperOptionSet::loadMultiple()) {
      $element['swiper_option_set'] = array(
        '#type' => 'entity_autocomplete',
        '#title' => t('Swiper option set'),
        '#target_type' => 'swiper_option_set',
        '#default_value' => $value ? SwiperOptionSet::load($value) : '',
        '#validate_reference' => FALSE,
        '#size' => 60,
        '#maxlength' => 60,
        '#description' => t('Select the swiper option set you would like to use for this field'),
      );
    }
    else {
      $element['no_sets_info'] = self::getNoOptionSetsAvailableInfo();
    }

    return $element;
  }

  /**
   * Returns a markup element for summary and settings form.
   *
   * @return array
   *   Render array of element that indicates that there aren't any option sets.
   */
  public static function getNoOptionSetsAvailableInfo() {
    $description_link = Link::fromTextAndUrl(
        t('Configure Swiper option sets'), Url::fromRoute('entity.swiper_option_set.collection')
    );
    return [
      '#type' => 'item',
      '#markup' => t('There are no Swiper option sets available currently.'),
      '#description' => $description_link->toRenderable() + [
      '#access' => \Drupal::currentUser()->hasPermission('administer swiper option sets')
      ],
    ];
  }

}
