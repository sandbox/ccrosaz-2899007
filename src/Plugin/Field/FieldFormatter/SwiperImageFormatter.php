<?php

/**
 * @file
 * Contains \Drupal\atix_swiper\Plugin\Field\FieldFormatter\SwiperImageFormatter.
 */

namespace Drupal\atix_swiper\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
//use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\atix_swiper\Entity\SwiperOptionSet;
use Drupal\atix_swiper\SwiperSlider;

/**
 * Plugin implementation of the image swiper field formatter.
 *
 * @FieldFormatter(
 *   id = "swiper_image_formatter",
 *   label = @Translation("Image Swiper"),
 *   description = @Translation("Renders multi value image field contents as Swiper slider."),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class SwiperImageFormatter extends ImageFormatter implements ContainerFactoryPluginInterface {
  /**
   * Constructs an SwiperImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   */
//  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $image_style_storage) {
//    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $current_user, $image_style_storage);
//  }

  /**
   * {@inheritdoc}
   */
//  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
//    return new static(
//      $plugin_id,
//      $plugin_definition,
//      $configuration['field_definition'],
//      $configuration['settings'],
//      $configuration['label'],
//      $configuration['view_mode'],
//      $configuration['third_party_settings'],
//      $container->get('current_user'),
//      $container->get('entity.manager')->getStorage('image_style')
//    );
//  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings() + [
      'caption' => FALSE,
      'swiper_option_set' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $settings = $this->getSettings();

    $element['caption'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Caption'),
      '#default_value' => $settings['caption'],
      '#description' => t('Enable caption, from the [title] ot the [alt] field of the image.'),
    );

    $element += SwiperSlider::getSettingForm($settings['swiper_option_set']);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $settings = $this->getSettings();

    if ($settings['caption']) {
      $summary[] = t('Caption');
    }

    // Check whether any option sets are available.
    if (SwiperOptionSet::loadMultiple()) {
      if ($settings['swiper_option_set']) {
        $swiper_option_set = SwiperOptionSet::load($settings['swiper_option_set']);
        $summary[] = t(
          'Swiper option set: @option_set', ['@option_set' => $swiper_option_set->label()]
        );
      }
      else {
        $summary[] = t('No Swiper option set selected');
      }
    }
    else {
      $summary[] = SwiperSlider::getNoOptionSetsAvailableInfo();
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $settings = $this->getSettings();

    // Modify each elements
    foreach ($elements as $delta => $element) {
      $elements[$delta]['#theme'] = 'image_swiper_formatter';
      $elements[$delta]['#caption'] = $settings['caption'];
    }

    // If there's more than one reference to display and an option set was
    // configured, add the Swiper library and some markup for the Swiper.
    if ($items->count() > 1 && $settings['swiper_option_set']) {
      /** @var \Drupal\atix_swiper\Entity\SwiperOptionSet $swiper_option_set */
      $swiper_option_set = SwiperOptionSet::load($settings['swiper_option_set']);
      // Prevent fatal error in case option set was deleted.
      if (!$swiper_option_set) {
        return $elements;
      }

      $field_instance = $items->getFieldDefinition();
      $settings['field_name'] = $field_instance->getName();
      $settings['field_type'] = $field_instance->getType();
      $settings['label_display'] = $this->label;

      // Create a key that allows fetching the view mode and field specific
      // option set in JS. This is necessary in order to support different
      // Swiper option sets for the same node that might be displayed multiple
      // times on a page in different view modes with different Swiper options.
      $settings['slider_id'] = SwiperSlider::getUniqueId($this->fieldDefinition->id() . '-' . $this->viewMode, 'field');

      $elements = array(
        '#theme' => 'images_swiper_formatter',
        '#children' => $elements,
        '#settings' => $settings,
        // This can be cached until the node or the option set will change.
        '#cache' => [
          'tags' => Cache::mergeTags(
            $swiper_option_set->getCacheTags(), $items->getEntity()->getCacheTags()
          ),
        ],
      );
    }
    
    return $elements;
  }

}
