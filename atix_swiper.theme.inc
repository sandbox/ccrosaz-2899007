<?php

/**
 * @file
 * Preprocessors and helper functions to make theming easier.
 */
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Template\Attribute;
use Drupal\atix_swiper\SwiperSlider;
use Drupal\atix_swiper\Entity\SwiperOptionSet;

/**
 * Prepares variables for views swiper slider templates.
 *
 * Default template: views-swiper-slider.html.twig.
 *
 * @param array $vars
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_views_swiper_slider(array &$vars) {
  $view = $vars['view'];
  $slider_id = SwiperSlider::getUniqueViewId($view);
  $options = $view->style_plugin->options;
  $vars['options'] = $options;
  $vars['slider_id'] = $slider_id;

  if ($options['swiper_option_set']) {
    /** @var \Drupal\atix_swiper\Entity\SwiperOptionSet $swiper_option_set */
    $swiper_option_set = SwiperOptionSet::load($options['swiper_option_set']);
    // Prevent fatal error in case option set was deleted.
    if ($swiper_option_set) {
      $parameters = $swiper_option_set->getParameters();
      $vars['controls'] = _swiper_slider_getControls($parameters);

      // Prepare attributes.
      $vars['attributes'] = new Attribute(['class' => 'swiper-container']);
      // These values are taken from the swiper option set form, but Attribute
      // escapes the values automatically using Html::escape, thus no need to escape
      // any values here.
      $vars['wrapper_attributes'] = new Attribute(['class' => $parameters['wrapperClass']]);
      $vars['slide_attributes'] = new Attribute(['class' => $parameters['slideClass']]);

      // Controls
      $vars['controls'] = _swiper_slider_getControls($parameters);

      // Attachements
      _swiper_slider_addAttachement($vars, $slider_id, $parameters);
    }
  }
}

/**
 * Prepares variables for swiper slider field templates.
 *
 * Default template: reference-swiper-formatter.html.twig.
 *
 * @param array $vars
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #id, #attributes, #children.
 */
function template_preprocess_reference_swiper_formatter(&$vars) {
  $element = $vars['element'];
  $options = $element['#settings'];
  $vars['options'] = $options;

//  // Copy some values into variables for easier access in template.
//  $delta = 0;
//  while (!empty($element['#children'][$delta])) {
//    $vars['children'][$delta] = $element['#children'][$delta];
//    $delta++;
//  }

  if ($options['swiper_option_set']) {
    /** @var \Drupal\atix_swiper\Entity\SwiperOptionSet $swiper_option_set */
    $swiper_option_set = SwiperOptionSet::load($options['swiper_option_set']);
    // Prevent fatal error in case option set was deleted.
    if ($swiper_option_set) {
      $parameters = $swiper_option_set->getParameters();
      
      // Copy some values into variables for easier access in template.
      $delta = 0;
      while (!empty($element['#children'][$delta])) {
        $vars['children'][$delta] = $element['#children'][$delta];
        $delta++;
      }
      
      _swiper_slider_addFieldVars($vars, $element, $parameters);
    }
  }
}

/**
 * Prepares variables for container templates.
 *
 * Default template: images-swiper-formatter.html.twig.
 *
 * @param array $vars
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #attributes, #children.
 */
function template_preprocess_images_swiper_formatter(&$vars) {
  $element = $vars['element'];
  $options = $element['#settings'];
  $vars['options'] = $options;

  if ($options['swiper_option_set']) {
    /** @var \Drupal\atix_swiper\Entity\SwiperOptionSet $swiper_option_set */
    $swiper_option_set = SwiperOptionSet::load($options['swiper_option_set']);
    // Prevent fatal error in case option set was deleted.
    if ($swiper_option_set) {
      $parameters = $swiper_option_set->getParameters();

      $vars['children'] = $element['#children'];

      _swiper_slider_addFieldVars($vars, $element, $parameters);
    }
  }
}

/**
 * Prepares variables for image swiper formatter templates.
 *
 * Default template: image-swiper-formatter.html.twig.
 *
 * @param array $vars
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - item_attributes: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - image_style: An optional image style.
 *   - url: An optional \Drupal\Core\Url object.
 * 
 * @todo See if possible to just use image formatter with a preprocess override
 */
function template_preprocess_image_swiper_formatter(&$vars) {
  if ($vars['image_style']) {
    $vars['image'] = array(
      '#theme' => 'image_style',
      '#style_name' => $vars['image_style'],
    );
  }
  else {
    $vars['image'] = array(
      '#theme' => 'image',
    );
  }
  $vars['image']['#attributes'] = $vars['item_attributes'];

  $item = $vars['item'];

  // Do not output an empty 'title' attribute.
  if (Unicode::strlen($item->title) != 0) {
    $vars['image']['#title'] = $item->title;
    $vars['caption_text'] = $item->title;
  }
  else {
    $vars['caption_text'] = $item->alt;
  }

  if (($entity = $item->entity) && empty($item->uri)) {
    $vars['image']['#uri'] = $entity->getFileUri();
  }
  else {
    $vars['image']['#uri'] = $item->uri;
  }

  foreach (array('width', 'height', 'alt') as $key) {
    $vars['image']["#$key"] = $item->$key;
  }
}

/**
 * 
 * @param array $parameters
 *  Swiper Slider Set parameters
 *   
 * @return array Attribute
 */
function _swiper_slider_getControls($parameters) {
  // Check for controls and add them to variables array if necessary.
  $controls = [
    'pagination' => 'pagination',
    'next_button' => 'nextButton',
    'prev_button' => 'prevButton',
    'scrollbar' => 'scrollbar',
  ];
  $elements = [];
  foreach ($controls as $control => $param_key) {
    if (!empty($parameters[$param_key])) {
      $elements[$control]['attributes'] = new Attribute([
        // Replace the dot in the CSS selector string of the control.
        'class' => [str_replace('.', '', $parameters[$param_key])],
      ]);
    }
  }
  return $elements;
}

/**
 * 
 *
 * @param array $vars
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #attributes, #children.
 * @param string $slider_id
 *   The unique Slider ID
 * @param array $parameters
 *   Swiper Slider Set parameters
 */
function _swiper_slider_addAttachement(&$vars, $slider_id, $parameters) {
  // Attach libraries.
  $vars['#attached']['library'][] = 'atix_swiper/jquery.swiper.slider';
  $vars['#attached']['library'][] = 'atix_swiper/atix.swiper';

  // Swiper settings
  $settings = [];
  // IDs.
  $settings['slider_id'] = $slider_id;

  // Swiper settings
  $settings['swiper'] = $parameters;

  // Attach settings.
  $vars['#attached']['drupalSettings']['atix_swiper'][$slider_id] = $settings;
}

/**
 * Prepares variables for container templates.
 *
 * @param array $vars
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #attributes, #children.
 * @param array $element
 *   The field element
 * @param array $parameters
 *   Swiper Slider Set parameters
 */
function _swiper_slider_addFieldVars(&$vars, $element, $parameters) {
  $vars['label_hidden'] = isset($element['#children']['#label_display']) && $element['#children']['#label_display'] == 'hidden';
  $vars['hashnav'] = isset($parameters['hashnav']) ? TRUE : FALSE;
  // Slider ID
  $slider_id = $element['#settings']['slider_id'];
  $vars['slider_id'] = $slider_id;

  // Prepare attributes.
  $vars['attributes'] = new Attribute();
  $vars['title_attributes'] = new Attribute();
  $vars['container_attributes'] = new Attribute(['class' => 'swiper-container']);
  // These values are taken from the swiper option set form, but Attribute
  // escapes the values automatically using Html::escape, thus no need to escape
  // any values here.
  $vars['wrapper_attributes'] = new Attribute(['class' => $parameters['wrapperClass']]);
  $vars['slide_attributes'] = new Attribute(['class' => $parameters['slideClass']]);

  // Controls
  $vars['controls'] = _swiper_slider_getControls($parameters);

  // Attachements
  _swiper_slider_addAttachement($vars, $slider_id, $parameters);
}
